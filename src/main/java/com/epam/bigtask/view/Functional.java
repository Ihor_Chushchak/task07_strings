package com.epam.bigtask.view;
import java.io.File;

@FunctionalInterface
public interface Functional {
    void start(File fileName);
}
