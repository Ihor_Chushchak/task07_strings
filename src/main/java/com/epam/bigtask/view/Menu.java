package com.epam.bigtask.view;

import com.epam.bigtask.Main;
import com.epam.bigtask.model.sentence.Sentence;
import com.epam.bigtask.model.word.Word;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Scanner;

import static com.epam.bigtask.Main.fileName;

public class Menu {

    private Locale locale;
    private ResourceBundle bundle;

    private LinkedHashMap<String, String> menu;
    private LinkedHashMap<String, Functional> methodsMenu;
    public static final Scanner input = new Scanner(System.in);
    public static Logger logger = LogManager.getLogger(com.epam.Main.class);


    public Menu() {
        initMenu();
    }


    private void setMenu() {
        menu = new LinkedHashMap<>();

        menu.put("1", bundle.getString("1"));
        menu.put("2", bundle.getString("2"));
        menu.put("3", bundle.getString("3"));
        menu.put("4", bundle.getString("4"));
        menu.put("5", bundle.getString("5"));
        menu.put("6", bundle.getString("6"));
        menu.put("7", bundle.getString("7"));
        menu.put("8", bundle.getString("8"));
        menu.put("9", bundle.getString("9"));
        menu.put("10", bundle.getString("10"));
        menu.put("11", bundle.getString("11"));
        menu.put("12", bundle.getString("12"));
        menu.put("13", bundle.getString("13"));
        menu.put("14", bundle.getString("14"));
        menu.put("15", bundle.getString("15"));
        menu.put("16", bundle.getString("16"));
        menu.put("17", bundle.getString("17"));
        menu.put("18", bundle.getString("18"));
        menu.put("19", bundle.getString("19"));
        menu.put("20", bundle.getString("20"));
        menu.put("21", bundle.getString("21"));
        menu.put("22", bundle.getString("22"));
        menu.put("23", bundle.getString("23"));
        menu.put("24", bundle.getString("24"));
        menu.put("30", bundle.getString("30"));
    }

    private void setMenuMethods() {
        methodsMenu = new LinkedHashMap<>();

        methodsMenu.put("1", Main::changeFile);
        methodsMenu.put("2", Main::checkCurrentFile);
        methodsMenu.put("3", Main::showSentences);
        methodsMenu.put("4", Main::showWords);
        methodsMenu.put("5", Sentence::showSentencesWithNotSimilarWords);
        methodsMenu.put("6", Sentence::showSentencesWithSimilarWordsOfSen);//task1
        methodsMenu.put("7", Sentence::getSortedByLengthSentences);//task2
        methodsMenu.put("8", Word::findWordsThatAreOnlyInFirstSentence);//task3
        methodsMenu.put("9", Word::findUniqueWordsInTheQuestionnaires);//task4
        methodsMenu.put("10", Sentence::swapFirstVowelAndLongestWords);//task5
        methodsMenu.put("11", Word::showSortedByFirstSymbol);//task6
        methodsMenu.put("12", Word::showSortedByVowelPercent);//task7
        methodsMenu.put("13", Word::showOnlyVowelSortByConsonant);//task8
        methodsMenu.put("14", Word::showSortedByCountOfSymbolToIncrease);//task9
        methodsMenu.put("15", Word::showSortedByDecreaseOfListWords);//task10
        methodsMenu.put("16", Sentence::showWithoutMaxWord);//task11
        methodsMenu.put("17", Word::showAfterDeletedWordsStartsConsonant);//task12
        methodsMenu.put("18", Word::showSortedByCountOfSymbolToDecrease);//task13
        methodsMenu.put("19", Word::showAllPalindromes);//task14
        methodsMenu.put("20", Word::showWordsAfterDeleteAllNextWordsThatStartWithFirstSymbol);//task15
        methodsMenu.put("21", Word::showAfterChangeAllWordsInputtedLength);//task16
        methodsMenu.put("22", this::internationalizeEnglish);
        methodsMenu.put("23", this::internationalizeMenuUkrainian);
        methodsMenu.put("24", this::internationalizeJapan);
        methodsMenu.put("30", this::exitFromMenu);
    }

    private void initMenu() {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("MyMenu_en", locale);

        setMenu();
        setMenuMethods();

    }

    private void internationalizeMenuUkrainian(File f) {
        locale = new Locale("uk");
        bundle = ResourceBundle.getBundle("MyMenu", locale);

        setMenu();
        show();
    }

    private void internationalizeEnglish(File f) {
        locale = new Locale("en");
        bundle = ResourceBundle.getBundle("MyMenu", locale);

        setMenu();
        show();
    }

    private void internationalizeJapan(File f) {
        locale = new Locale("ja");
        bundle = ResourceBundle.getBundle("MyMenu", locale);

        setMenu();
        show();
    }


    private void outputMenu() {
        logger.info("\nMENU:\n");
        menu.forEach((k, v) -> System.out.println(v));
    }

    private void exitFromMenu(File fName) {
        System.exit(0);
    }

    public void show() {
        String keyMenu;
        while (true) {
                logger.info("\n\n");
                outputMenu();
                logger.info("\nPlease, select menu point: ");
                keyMenu = input.nextLine();
                logger.info("\n\n\n");
                methodsMenu.get(keyMenu).start(fileName);
        }

    }
}
