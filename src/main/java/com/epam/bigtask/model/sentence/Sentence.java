package com.epam.bigtask.model.sentence;


import java.io.File;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.epam.bigtask.model.FileSettings.*;
import static com.epam.bigtask.model.punctuation.Punctuation.removeBadSymbols;
import static com.epam.bigtask.model.word.Word.getWords;
import static com.epam.bigtask.model.word.Word.getWordsWithAmount;
import static com.epam.bigtask.Main.showList;
import static com.epam.bigtask.view.Menu.logger;

public class Sentence {
    public static List<String> getSentence(String text) {
        String[] sents = text.split(" *[?!.;] *");
        sents = removeNullOrEmpty(sents);
        return Arrays.stream(sents)
                .map(a -> a = a.toLowerCase())
                .collect(Collectors.toList());
    }

    public static List<String> getQuestionSentences(String text) {
        String[] sents = text.split(" *[\\w ]+[!.] *");
        sents = removeNullOrEmpty(sents);
        return new ArrayList<>(Arrays.asList(sents));
    }

    public static void showSentencesWithNotSimilarWords(File fName) {
        String text = getTextFromFile(fName);

        List<String> sents = getSentence(text);

        for (String wordW2 : sents) {
            List<String> list = getWords(wordW2);

            //set unique word, then  his count find
            Set<String> st = new HashSet<>(list);
            boolean isSimilarWords = st.stream()
                    .anyMatch(word -> Collections.frequency(list, word) > 1);
           if (!isSimilarWords) {
               logger.info("\n"+wordW2);
            }
        }
    }

    public static void showSentencesWithSimilarWordsOfSen(File fName) {
        String text = getTextFromFile(fName);
        List<String> sentences = getSentence(text);
        Map<String, Integer> wordAmount = getWordsWithAmount(text);

        Map<String, Integer> forCycle = new HashMap<>(wordAmount);
        for (Map.Entry<String, Integer> val : forCycle.entrySet()) {
            if (val.getValue() == 1) {
                wordAmount.remove(val.getKey(), val.getValue());
            }
        }

        for (String sentence : sentences) {
            for (String value : wordAmount.keySet()) {
                if (sentence.contains(value)) {
                    logger.info("\n"+sentence);
                    break;
                }
            }
        }
    }

    public static void getSortedByLengthSentences(File fName) {
        String text = getTextFromFile(fName);
        getSentence(text).stream()
                .sorted(Comparator.comparing(String::length))
                .forEach(logger::info);
    }

    public static void swapFirstVowelAndLongestWords(File fName) {
        String text = getTextFromFile(fName);
        List<String> sentences = getSentence(text);

        Pattern patternVowel = Pattern.compile(startWithVowel);
        for (String sentence : sentences) {
            List<String> words = getWords(sentence);

            Matcher matcher = patternVowel.matcher(sentence);

            String vowelWord = "";
            if (matcher.find()) {
                vowelWord = sentence.substring(matcher.start(), matcher.end());
                logger.info("\nVowel word: " + sentence.substring(matcher.start(), matcher.end()));
            }
            if (!vowelWord.isEmpty()) {
                Optional<String> longestOptWord = words
                        .stream()
                        .max(Comparator.comparing(String::length));
                String longestWord;
                if (longestOptWord.isPresent()) {
                    longestWord = longestOptWord.get();
                    logger.info("\nLongest word: " + longestWord);
                    int indexMaxWord = words.indexOf(longestWord);
                    int indexVowel = words.indexOf(vowelWord);

                    showList(words, "Before: ");

                    words.set(indexMaxWord, vowelWord);
                    words.set(indexVowel, longestWord);

                    showList(words, "After: ");
                }
            }
        }
    }

    public static void showWithoutMaxWord(File fName) {
        String text = getTextFromFile(fName);
        List<String> sentences = getSentence(text);

        logger.info("\nInput symbol symbol of which should start word");
        String firstSymbol = getInputtedSymbol();

        logger.info("\nInput symbol symbol of which should end word");
        String lasSymbol = getInputtedSymbol();

        Pattern p = Pattern.compile("\\b" + firstSymbol + "[\\w']+" + lasSymbol + "\\b");
        for (String sentence : sentences) {
            Matcher m = p.matcher(sentence);

            List<String> foundedWords = new LinkedList<>();
            while (m.find()) {
                foundedWords.add(m.group());
            }
            if (!foundedWords.isEmpty()) {
                String max = Collections.max(foundedWords, Comparator.comparing(String::length));
                logger.info("\nBefore: " + sentence);
                logger.info("\nMax: " + max);
                sentence = removeBadSymbols(sentence.replace(max, ""));
                logger.info("\nAfter: " + sentence + "\n");
            }
        }
    }
}
