package com.epam.bigtask.model.word;


import java.io.File;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.epam.bigtask.Main.inputInteger;
import static com.epam.bigtask.model.FileSettings.getInputtedSymbol;
import static com.epam.bigtask.model.FileSettings.getTextFromFile;
import static com.epam.bigtask.model.FileSettings.startWithVowel;
import static com.epam.bigtask.model.punctuation.Punctuation.removeBadSymbols;
import static com.epam.bigtask.model.sentence.Sentence.getQuestionSentences;
import static com.epam.bigtask.model.sentence.Sentence.getSentence;
import static com.epam.bigtask.view.Menu.input;
import static com.epam.bigtask.view.Menu.logger;


public class Word {

    public static List<String> getWords(String text) {
        text = text.trim();
        String[] words = text.split("[ .!?]+");
        return Arrays.stream(words)
                .map(a -> a = a.toLowerCase())
                .collect(Collectors.toList());
    }

    public static Map<String, Integer> getWordsWithAmount(String text) {
        List<String> words = getWords(text);

        Map<String, Integer> map = new HashMap<>();
        for (String s : words) {
            Integer i = map.get(s);
            map.put(s, (i == null) ? 1 : i + 1);
        }
        return map;
    }

    public static List<String> getUniqueWords(String text) {
        return getWords(text)
                .stream()
                .distinct()
                .collect(Collectors.toList());
    }


    public static void findWordsThatAreOnlyInFirstSentence(File fName) {
        String text = getTextFromFile(fName);
        String firstSentence = getSentence(text).get(0);
        text = text.replace(firstSentence, "");

        List<String> firstSenWords = getWords(firstSentence);
        Set<String> anotherWords = new HashSet<>(getWords(text));

        firstSenWords.removeAll(anotherWords);
        logger.info("\n" + firstSenWords);
    }

    public static void findUniqueWordsInTheQuestionnaires(File fName) {
        String text = getTextFromFile(fName);
        List<String> questSen = getQuestionSentences(text);
        logger.info("\nSentences: " + questSen + "\n");
        List<String> words = new ArrayList<>();
        questSen.forEach(a -> words.addAll(getUniqueWords(a)));
        logger.info("\nWords: " + words + "\n");
        int inputLength = inputInteger();
        logger.info("\nUnique words with length " + inputLength + ": "
                + words.stream()
                .filter(a -> a.length() == inputLength)
                .collect(Collectors.toList())
        );
    }

    public static void showSortedByFirstSymbol(File fName) {
        String text = getTextFromFile(fName);
        List<String> words = getWords(text);

        Collections.sort(words);
        String previousLetter = "";
        for (String word : words) {
            if (!word.startsWith(previousLetter)) {
                logger.info("\n");
                ;
            }
            logger.info("\n" + word);

            previousLetter = String.valueOf(word.charAt(0));
        }
    }

    public static double getPercentVowel(String word) {
        Pattern p = Pattern.compile("[eyuioaEYUIOA]");
        Matcher m = p.matcher(word);

        int count = 0;
        while (m.find()) {
            count++;
        }
        return count == 0 ? 0 : (count * 1. / word.length());
    }

    public static void showSortedByVowelPercent(File fName) {
        String text = getTextFromFile(fName);
        getWords(text)
                .stream()
                .sorted(Comparator.comparing(Word::getPercentVowel))
                .forEach(a -> logger.info("\n" + a + "  \t\t = " + (getPercentVowel(a) * 100) + "%"))
        ;
    }

    public static void showOnlyVowelSortByConsonant(File fName) {
        String text = getTextFromFile(fName);

        Pattern p1 = Pattern.compile(startWithVowel);
        Matcher m1 = p1.matcher(text);

        Pattern p2 = Pattern.compile("[^eyuioaEYUIOA]");
        Map<String, String> wordsVowel = new HashMap<>();
        while (m1.find()) {
            Matcher m2 = p2.matcher(m1.group());
            if (m2.find()) {
                wordsVowel.put(m1.group(), m2.group());
            } else {
                wordsVowel.put(m1.group(), "~");
            }
        }
        logger.info("\nBefore sorting: ");
        wordsVowel.keySet().forEach(logger::info);
        logger.info("\n\n\n");
        logger.info("\nAfter sorting: ");
        Map<String, String> sorted = wordsVowel.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue())
                .collect(
                        Collectors.toMap(
                                Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new
                        ));
        sorted.keySet().forEach(logger::info);
    }

    public static int getAmountOfEntrancesSymbol(String word, String symbol) {
        Pattern p = Pattern.compile(symbol);
        Matcher m = p.matcher(word);

        int count = 0;
        while (m.find()) {
            count++;
        }
        return count;
    }

    public static Map<String, Integer> getSortedByKeyThenByValueToIncrease(Map<String, Integer> map) {
        return map
                .entrySet()
                .stream()
                .sorted(Map.Entry.comparingByKey())
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (k, v) -> v, LinkedHashMap::new))
                ;
    }

    public static Map<String, Integer> getSortedByKeyThenByValueToDecrease(Map<String, Integer> map) {
        return map
                .entrySet()
                .stream()
                .sorted(Map.Entry.comparingByKey())
                .sorted(Map.Entry.comparingByValue())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (k, v) -> v, LinkedHashMap::new))
                ;
    }

    public static void showSortedByCountOfSymbolToIncrease(File fName) {
        Map<String, Integer> map = combined9and13task(fName);
        getSortedByKeyThenByValueToIncrease(map)
                .forEach((k, v) -> logger.info("\n" + k + "=" + v));
    }

    public static void showSortedByDecreaseOfListWords(File fName) {
        String text = getTextFromFile(fName);
        List<String> list = new ArrayList<>(Arrays.asList("the", "of", "from", "i", "to", "as", "but"));
        List<String> sentences = getSentence(text);
        for (String sentence : sentences) {
            List<String> words = getWords(sentence);
            words.retainAll(list);
            Map<String, Integer> wordsAmount;

            logger.info("\nSentence :" + sentence);
            logger.info("\nWords from list: " + words);
            String allWordsFromList = removeBadSymbols(words.toString() + list.toString());
            wordsAmount = getWordsWithAmount(allWordsFromList);

            wordsAmount.forEach((k, v) -> wordsAmount.put(k, v - 1));

            logger.info("\nWords: " + getSortedByKeyThenByValueToDecrease(wordsAmount) + "\n");
        }
    }

    public static void showAfterDeletedWordsStartsConsonant(File fName) {
        String text = getTextFromFile(fName);
        int lenDelete = inputInteger();

        Pattern p = Pattern.compile("\\b[\\w&&[^eyuioaEYUIOA]][\\w']{" + (lenDelete - 1) + "}\\b");
        Matcher m = p.matcher(text);

        List<String> wordsAll = getWords(text);
        Set<String> deleteWords = new HashSet<>();
        while (m.find()) {
            deleteWords.add(m.group());
        }
        logger.info("\nDeleted words: " + deleteWords + "\n");
        wordsAll.removeAll(deleteWords);
        logger.info("\nAll words: " + wordsAll);
    }

    public static Map<String, Integer> combined9and13task(File fName) {
        String text = getTextFromFile(fName);
        List<String> words = getWords(text);
        Map<String, Integer> map = new HashMap<>();

        logger.info("\nInput symbol by which you want sort words");
        String symbol = getInputtedSymbol();

        for (String word : words) {

            int amountOfEntrances = getAmountOfEntrancesSymbol(word, symbol);
            map.put(word, amountOfEntrances);
        }

        logger.info("\nSorted words by " + symbol + ": ");
        return map;
    }

    public static void showSortedByCountOfSymbolToDecrease(File fName) {
        Map<String, Integer> sorted = combined9and13task(fName);
        getSortedByKeyThenByValueToDecrease(sorted)
                .forEach((k, v) -> logger.info("\n" + k + "=" + v));
    }

    public static boolean isPalindrome(String word) {
        return word.equals(new StringBuilder(word).reverse().toString());
    }

    public static void showAllPalindromes(File fName) {
        String text = getTextFromFile(fName);

        logger.info("\nPalindromes:[ ");
        getUniqueWords(text).stream()
                .filter(Word::isPalindrome)
                .forEach(a -> System.out.println(" " + a));
        logger.info("\n].");
    }

    public static void showWordsAfterDeleteAllNextWordsThatStartWithFirstSymbol(File fName) {
        String text = getTextFromFile(fName);
        List<String> words = getWords(text);

        int sizeWords = words.size();

        logger.info("\nWords before: " + words);
        for (int i = 0; i < sizeWords; i++) {
            for (int j = i + 1; j < sizeWords; j++) {
                if (words.get(i).charAt(0) == words.get(j).charAt(0)) {
                    words.remove(j);
                    sizeWords--;
                }
            }
        }
        logger.info("\nWords after: " + words);
    }

    public static void showAfterChangeAllWordsInputtedLength(File fName) {
        String text = getTextFromFile(fName);

        int len = inputInteger();
        logger.info("\nInput string that you want replace on words inputted length:");
        String newWord = input.nextLine();

        List<String> sentences = getSentence(text);
        logger.info("\nBefore: ");
        sentences.forEach(logger::info);

        sentences.replaceAll(a -> a.replaceAll("\\b[\\w']{" + len + "}\\b", newWord));
        logger.info("\n\n\nAfter: ");
        sentences.forEach(logger::info);
    }
}
