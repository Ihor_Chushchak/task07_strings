package com.epam.bigtask.model.punctuation;

public class Punctuation {
    public static String removeBadSymbols(String everything) {
        String patNotOnlyDigit = " *[\\d ]+[.?!]";
        everything = everything.replaceAll(patNotOnlyDigit, "");
        return everything.replaceAll("[ ,\":+\\-=*₴()<>&%@\\]\\[]+", " ").toLowerCase();
    }
}
