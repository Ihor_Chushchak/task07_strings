package com.epam.bigtask;

import com.epam.bigtask.view.Menu;

import java.io.File;
import java.util.List;

import static com.epam.bigtask.model.FileSettings.getTextFromFile;
import static com.epam.bigtask.model.sentence.Sentence.getSentence;
import static com.epam.bigtask.model.word.Word.getWords;
import static com.epam.bigtask.view.Menu.input;
import static com.epam.bigtask.view.Menu.logger;


public class Main {
    static final String file = "ForFastCheck.TXT";

    public static File fileName = new File(file);

    public static void changeFile(File fName) {
        logger.info("\nInput the location of the new file: ");
        String newName = input.nextLine();

        File fNew = new File(newName);
        if (fNew.isFile()) {
            logger.info("\nFine, we found this file!");
            fileName = fNew;
        } else {
            logger.info("\nError..\nCan't find this file!");
            logger.info("\nFile: " + newName);
        }
    }


    public static void showSentences(File fName) {
        String text = getTextFromFile(fName);
        getSentence(text).forEach(logger::info);
    }

    public static void showWords(File fName) {
        String text = getTextFromFile(fName);
        getWords(text).forEach(logger::info);
    }

    public static void checkCurrentFile(File fName) {
        String everything = getTextFromFile(fName);
        logger.info("\nFILE:\n\n " + everything);
    }

    public static int inputInteger() {
        int value;
        while (true) {
            logger.info("\nInput length: ");
            String inputInt = input.nextLine();
            try {
                value = Integer.parseInt(inputInt);
                break;
            } catch (Exception e) {
                logger.info("\nError\nPlease try again!\n");
            }
        }
        logger.info("\n");
        return value;
    }

    public static void showList(List<String> words, String mess) {
        logger.info(mess);
        words.forEach(a -> logger.info(a + " "));
        logger.info("\n");
    }

    public static void main(String[] args) {
        new Menu().show();
    }
}
