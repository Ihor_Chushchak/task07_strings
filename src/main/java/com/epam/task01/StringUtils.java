package com.epam.task01;

import com.epam.Main;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class StringUtils {
    private static Logger logger = LogManager.getLogger(Main.class);

    String foo(Object... args) {
        String string = "";
        for (Object arg : args) {
            string = arg.toString() + "; ";
        }
        logger.info(string);
        return string;
    }
}
